## Was: QAAFI Intensive Advanced R Course Nov 26 2018: R Markdown Session
## Now User2019 talk although a better strategy might be
##  to roduce tex from Rmd then R CMD latexmk the texfile
##
## NB: 'gmake' works for handouts but standard macOS 'make' DOES NOT
##     Currently, macOS make is GNU Make 3.81 (c 2006) which is
##     relatively prehstoric. However, gmake is 4.2.1 (c 2016) which is 
##     the latest                           Peter Baker 15 July 2019

BASENAME=peterBaker_UseR2019
IMAGES=images/*.png images/*.jpg
## IMAGES=images/*.png
BIBFILE=r_courses.bib
SETUP_TEX=include/doc-prefix.tex include/doc-suffix.tex include/preamble.tex

.PHONY: all
all: ${BASENAME}_beamer.pdf

include ~/lib/r-rules.mk

 ${BASENAME}_beamer.pdf:  ${BASENAME}.Rmd ${BIBFILE} ${SETUP_TEX} ${IMAGES}

.PHONY: handouts
handouts: ${BASENAME}_beamer-handout.pdf  ${BASENAME}_beamer-4up.pdf

.PHONY: zipfile
zipfile:
	zip -9 ${BASENAME}.zip ${BASENAME}_beamer-handout.pdf

## Next rule is unecessary here but I've left in as example of
## semi-automated workflow when I really should be using a globally
## accessible bib file in ~/texmf but that would be problematic since
## outside of git repo

## use this with care - perhaps meld better!
## r_courses.bib: ../../common_files/r_courses.bib
##	cp -Rap ../../common_files/r_courses.bib .
## https://unix.stackexchange.com/questions/100786/why-does-diff-fail-when-invoked-from-a-makefile
## NB: diff returns non-zero error code if a difference found so need to use
##     line -diff
.PHONY: bib
bib:
	-diff r_courses.bib ../../common_files/r_courses.bib
	@echo "If diffs then best to use:"
	@echo "  meld r_courses.bib ../../common_files/r_courses.bib &"
