**My UseR!2019 talk on gnumaker at Toulouse 10 July 2019**

Download the pdf or clone the source

This repository contains several versions of source and pdf that you don't need
so you could clone with

`git clone --depth=1 git@gitlab.com:petebaker/gnumaker-user2019-talk.git`

For details of GNU Make rules, see draft paper at [petebaker.id.au](http://petebaker.id.au/)

Peter
15 July 2019

NB: On macOS, use a newer GNU Make so install 'gmake' from homebrew for better
    results. I thought RStudio might help (hence one of the commit messages) but
    really, for handouts, I just need GNU Make which isn't 13 years old e.g.
`gmake handouts`